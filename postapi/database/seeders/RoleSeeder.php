<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate(); 
        
        $data = [
            ['name' => 'Użytkownik'], 
            ['name' => 'Redaktor'], 
            ['name' => 'Administrator']
        ]; 

        Role::insert($data); 
    }
}
