<?php

namespace App\Interfaces\Post;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;

interface PostRepositoryInterface 
{
    public function getAllPosts():Collection;
    public function getPostById(int $postId):Post;
    public function createPost(StorePostRequest $storePostRequest):Post;
    public function updatePost(int $postId, UpdatePostRequest $updatePostRequest):Post;
    public function deletePost(int $postId);
}