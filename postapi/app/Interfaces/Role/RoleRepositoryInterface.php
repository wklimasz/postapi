<?php

namespace App\Interfaces\Role;

interface RoleRepositoryInterface 
{
    public function getAllRoles();
    public function createRole(array $data);
}