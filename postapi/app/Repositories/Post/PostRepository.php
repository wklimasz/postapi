<?php

namespace App\Repositories\Post;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Interfaces\Post\PostRepositoryInterface;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;

class PostRepository implements PostRepositoryInterface 
{
    public function getAllPosts():Collection
    {
        $posts = Post::with('author:id, name, email.role:id,name')
            ->get(); 

            return $posts; 
    }

    public function getPostById(int $postId):Post
    {
        $post = Post::with('author:id, name, email.role:id,name')
            ->find($postId); 

        return $post; 
    }

    public function createPost(StorePostRequest $storePostRequest):Post
    {
        $post = Post::create($storePostRequest->validated());
        
        return $post; 
    }

    public function updatePost(int $postId, UpdatePostRequest $updatePostRequest):Post
    {
        $post = Post::find($postId)
            ->update($updatePostRequest->validated()); 

        return $post; 
    }

    public function deletePost(int $postId)
    {
        Post::find($postId)
            ->delete(); 
    }
}